#!/usr/bin/env bash

pip uninstall -y phoenixdb==0.7
pip uninstall -y JayDeBeApi
pip uninstall -y pyPhoenix
pip install --no-cache-dir git+https://ninel_hodzic@bitbucket.org/datazup/jaydebeapi.git
pip install --no-cache-dir git+https://ninel_hodzic@bitbucket.org/datazup/pyphoenix.git
