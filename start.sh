#!/usr/bin/env bash

echo "Init superset = $1"

echo "Executing npm run ..."
cd ~/superset/superset/assets && yarn && npm run sync-backend && npm run prod

echo "Executing setup ... "
cd ~/superset && python setup.py build
cd ~/superset && python setup.py install

echo "Starting db upgrade ..."
superset db upgrade

if [ $1 = "true" ]; then
    echo "Creating admin user ..."
    fabmanager create-admin --app superset --username admin --firstname Admin --lastname Admin --email admin@admin.com --password datazup17?
    superset init
fi

echo "Starting superset server ..."
superset runserver