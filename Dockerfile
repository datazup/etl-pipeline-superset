FROM ubuntu:16.04
MAINTAINER "Dino Causevic"

ENV INIT_SUPERSET="false"

RUN apt-get update -y
RUN apt-get install -y wget curl lsof git python-pip libmysqlclient-dev build-essential libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update
RUN apt-get install -y nodejs
RUN node -v
RUN npm -v
RUN npm install -g yarn
RUN mkdir ~/phoenix
RUN cd ~/phoenix && wget https://archive.apache.org/dist/phoenix/apache-phoenix-4.10.0-HBase-1.1/bin/apache-phoenix-4.10.0-HBase-1.1-bin.tar.gz
RUN cd ~/phoenix && tar zxvf apache-phoenix-4.10.0-HBase-1.1-bin.tar.gz
RUN rm -f ~/phoenix/apache-phoenix-4.10.0-HBase-1.1-bin.tar.gz

ENV CLASSPATH "/root/phoenix/apache-phoenix-4.10.0-HBase-1.1-bin/bin:/etc/hbase/conf:/root/phoenix/apache-phoenix-4.10.0-HBase-1.1-bin/bin../phoenix-4.10.0-HBase-1.1-client.jar:::/etc/hadoop/conf:/usr/local/Cellar/hadoop/2.7.3/libexec/etc/hadoop:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/common/lib/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/common/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/hdfs:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/hdfs/lib/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/hdfs/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/yarn/lib/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/yarn/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/mapreduce/lib/*:/usr/local/Cellar/hadoop/2.7.3/libexec/share/hadoop/mapreduce/*:/contrib/capacity-scheduler/*.jar"
ENV PG_HOST "superset-postgres"
ENV LOG4J "file:/root/phoenix/apache-phoenix-4.10.0-HBase-1.1-bin/bin/log4j.properties"
ENV DEBIAN_FRONTEND noninteractive

RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update -y
RUN echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
RUN apt-get install -y oracle-java8-installer

RUN mkdir ~/superset
RUN cd ~ && git clone https://bitbucket.org/datazup/etl-pipeline-superset.git superset
#https://dincausgmailcom:datazup17%3F@helixteamhub.cloud/datazup/projects/etlpipes/repositories/git/superset
RUN pip install --upgrade pip

RUN chmod +x ~/superset/pip_install.sh && pip install --no-cache-dir -r ~/superset/requirements.txt
RUN ~/superset/pip_install.sh

COPY start.sh /root/superset/start.sh
RUN chmod +x /root/superset/start.sh

ENTRYPOINT /root/superset/start.sh $INIT_SUPERSET